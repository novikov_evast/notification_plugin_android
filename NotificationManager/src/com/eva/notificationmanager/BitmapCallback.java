package com.eva.notificationmanager;


import android.graphics.Bitmap;

public interface BitmapCallback {
    void onSuccess(Bitmap bitmap);
    void onFail(String message);
}
