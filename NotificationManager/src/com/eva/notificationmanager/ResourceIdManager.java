package com.eva.notificationmanager;

import java.util.HashMap;

import android.util.Log;

public class ResourceIdManager {
	public static final String DRAWABLE = "drawable";
	public static final String DEFAULT_ICON = "app_icon";
	
	
	public static final String SMALL_ICON_KEY = "SmallIcon";
	public static final String TITLE_KEY = "ContentTitle";
	public static final String TEXT_KEY = "ContentText";
	public static final String AUTO_CANCEL_KEY = "AutoCancel";
	public static final String SOUND_KEY = "Sound";
	public static final String ID_KEY = "Id";
	public static final String LARGE_ICON_KEY = "LargeIcon";
	
	private static HashMap<String, String> _iconsMap;
	
	public static void AddIconToMap(String key, String value)
	{
		try {
			if(_iconsMap != null) {
				_iconsMap = new HashMap<String, String>();
			}
			if(_iconsMap.containsKey(key)){
				_iconsMap.put(key, value);
			}
		}
		catch(Exception e){
			Log.e("com.eva.notificationmanager", "failed to insert to map" , e);
		}
	}
	
	public static String GetIconForId(String id){
		if(_iconsMap != null && _iconsMap.containsKey(id)){
			return _iconsMap.get(id);
		}
		return DEFAULT_ICON;
	}
	
}
