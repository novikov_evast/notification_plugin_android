package com.eva.notificationmanager.local;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class ClearScheduleRunnable extends SuperScheduleRunnable implements Runnable {

	public ClearScheduleRunnable(Activity activity){
		super(activity);
	}
	
	@Override
	public void run() {
		AlarmManager alarmManager = (AlarmManager) mActivity.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(mActivity, AlarmManagerBroadcastReceiver.class);
		PendingIntent pendingIntent;
		pendingIntent = PendingIntent.getBroadcast(mActivity, ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		alarmManager.cancel(pendingIntent);
	}

}
