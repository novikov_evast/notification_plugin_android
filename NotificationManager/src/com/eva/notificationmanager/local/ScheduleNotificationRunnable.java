package com.eva.notificationmanager.local;

import org.json.JSONObject;

import com.eva.notificationmanager.ResourceIdManager;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class ScheduleNotificationRunnable extends SuperScheduleRunnable implements Runnable {

	private final String mJson;
	private final int mTime;
	
	public ScheduleNotificationRunnable(String json, Activity activity, int time) {
		super(activity);
		mJson = json;
		mTime = time;
	}
	
	@Override
	public void run() 
	{
		try {
			JSONObject jsonObject = new JSONObject(mJson);
			Bundle extras = new Bundle();
			
			addStingToExtras(jsonObject, ResourceIdManager.TITLE_KEY, extras);
			addStingToExtras(jsonObject, ResourceIdManager.TEXT_KEY, extras);
			addBooleanToExtras(jsonObject, ResourceIdManager.AUTO_CANCEL_KEY, extras);
			addStingToExtras(jsonObject, ResourceIdManager.SMALL_ICON_KEY, extras);
			addStingToExtras(jsonObject, ResourceIdManager.SOUND_KEY, extras);
			addStingToExtras(jsonObject, ResourceIdManager.LARGE_ICON_KEY, extras);
			addIntegerToExtras(jsonObject, ResourceIdManager.ID_KEY, extras);
	
			Intent intent = new Intent(mActivity, AlarmManagerBroadcastReceiver.class);
			intent.putExtras(extras);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(mActivity, ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			AlarmManager alarmManager = (AlarmManager) mActivity.getSystemService(Context.ALARM_SERVICE);
			alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000 * mTime, pendingIntent);
		}
		catch (Exception e) {
			Log.e(mActivity.getPackageName(), "schedule notification", e);
		}
	}

	private void addStingToExtras(JSONObject jsonObject, String name, Bundle extras) {
		if (jsonObject.has(name)) {
			try {
				Object obj = jsonObject.get(name);
				if (obj instanceof String) {
					extras.putString(name, (String) obj);
				}
			} catch (Exception e) {
				Log.e(getClass().getName(), "add String to extras", e);
			}
		}
	}

	private void addBooleanToExtras(JSONObject jsonObject, String name, Bundle extras) {
		if (jsonObject.has(name)) {
			try {
				Object obj = jsonObject.get(name);
				if (obj instanceof Boolean) {
					extras.putBoolean(name, (Boolean) obj);
				}
			} catch (Exception e) {
				Log.e(getClass().getName(), "add Boolean to extras", e);
			}
		}
	}

	private void addIntegerToExtras(JSONObject jsonObject, String name, Bundle extras) {
		if (jsonObject.has(name)) {
			try {
				Object obj = jsonObject.get(name);
				if (obj instanceof Integer) {
					extras.putInt(name, (Integer) obj);
				}
			} catch (Exception e) {
				Log.e(getClass().getName(), "add Integer to extras", e);
			}
		}
	}
}
