package com.eva.notificationmanager.local;

import java.util.Random;

import com.eva.notificationmanager.NotificationCreator;
import com.eva.notificationmanager.ResourceIdManager;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class AlarmManagerBroadcastReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, context.getPackageName());
		try {
			wakeLock.acquire();
			Bundle extras = intent.getExtras();
			if (extras != null) {
				NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
	
				NotificationCreator creator = new NotificationCreator(context, builder, extras);
				creator.Create();
				
				int id = getId(extras, context);
				NotificationManager service = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				service.notify(id, builder.build());
			}
		} catch(Exception e){
			Log.e(context.getPackageName(), "failed to create notification", e);
		}
		finally{
			wakeLock.release();
		}
	}

	private int getId(Bundle extras, Context context) {
		try {
			if (extras.containsKey(ResourceIdManager.ID_KEY)) {
				Object obj = extras.get(ResourceIdManager.ID_KEY);
				if (obj instanceof Integer) {
					return (Integer) obj;
				}
			}
			return randInt(51, Integer.MAX_VALUE);
		} catch (Exception e) {
			Log.e(context.getPackageName(), "set id", e);
			return 0;
		}
	}

	private int randInt(int min, int max) {
		Random rand = new Random();
		return rand.nextInt((max - min) + 1) + min;
	}
}
