package com.eva.notificationmanager.local;

import android.app.Activity;

public class NotificationScheduler {

	public void scheduleNotification(Activity activity, int timeInSeconds, String json) {
		activity.runOnUiThread(new ScheduleNotificationRunnable(json, activity, timeInSeconds));
	}

	public void clearSchedule(final Activity activity) {
		activity.runOnUiThread(new ClearScheduleRunnable(activity));
	}
}
