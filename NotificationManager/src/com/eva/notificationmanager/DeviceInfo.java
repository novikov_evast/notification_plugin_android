package com.eva.notificationmanager;

import android.content.Context;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import java.math.BigInteger;
import java.security.MessageDigest;

public class DeviceInfo {
    public static String getHardWareId(Context context) {
        String id = "";
        String androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        String imei = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
        String serial = android.os.Build.SERIAL;
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(androidId)) sb.append(androidId);
        if (!TextUtils.isEmpty(imei)) sb.append(imei);
        if (!TextUtils.isEmpty(serial)) sb.append(serial);
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(sb.toString().getBytes("UTF-8"));
            BigInteger bigInt = new BigInteger(1, md.digest());
            id = bigInt.toString(16);
            while (id.length() < 32) {
                id = "0" + id;
            }
        } catch (Exception ignored) {
        }
        return id;
    }
}
