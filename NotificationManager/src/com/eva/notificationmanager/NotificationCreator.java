package com.eva.notificationmanager;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class NotificationCreator {
	
	private final Context mContext;
	private final NotificationCompat.Builder mBuilder;
	private final Bundle mExtras;
	
	public NotificationCreator(Context context, NotificationCompat.Builder builder, Bundle extras){
		mContext = context;
		mBuilder = builder;
		mExtras = extras;
	}
	
	public void Create(){
		setSmallIcon();
		setContentTitle();
		setContentText();
		setAutoCancel();
		setContentIntent();
		setSound();
		setLargeIcon();
	}
	
	private void setSmallIcon() {
		try {
			String resourceName = getStringFromBundle(ResourceIdManager.SMALL_ICON_KEY, ResourceIdManager.DEFAULT_ICON);
			Log.d(mContext.getPackageName(), resourceName);
			int id = mContext.getResources().getIdentifier(resourceName, ResourceIdManager.DRAWABLE, mContext.getPackageName());
			if (id != 0) {
				mBuilder.setSmallIcon(id);
			} else {
				Log.e(mContext.getPackageName(), "resource not exitst: " + resourceName);
			}
		} catch (Exception e) {
			Log.e(mContext.getPackageName(), "set small icon", e);
		}
	}

	private void setContentTitle() {
		try {
			mBuilder.setContentTitle(getStringFromBundle(ResourceIdManager.TITLE_KEY, "DefaultTitle"));
		} catch (Exception e) {
			Log.e(mContext.getPackageName(), "set title", e);
		}
	}

	private void setContentText() {
		try {
			mBuilder.setContentText(getStringFromBundle(ResourceIdManager.TEXT_KEY, "DefaultText"));
		} catch (Exception e) {
			Log.e(mContext.getPackageName(), "set text", e);
		}
	}

	private void setAutoCancel() {
		try {
			boolean autoCancel = true;
			if (mExtras.containsKey(ResourceIdManager.AUTO_CANCEL_KEY)) {
				Object obj = mExtras.get(ResourceIdManager.AUTO_CANCEL_KEY);
				if (obj instanceof Boolean) {
					autoCancel = (Boolean) obj;
				}
			}
			mBuilder.setAutoCancel(autoCancel);
		} catch (Exception e) {
			Log.e(mContext.getPackageName(), "set auto cancel", e);
		}
	}

	private void setContentIntent() {
		try {
			Intent intent = mContext.getPackageManager().getLaunchIntentForPackage(mContext.getPackageName());
			PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_ONE_SHOT);
			mBuilder.setContentIntent(pendingIntent);
		} catch (Exception e) {
			Log.d(mContext.getPackageName(), "set pending intent", e);
		}
	}

	private void setSound() {
		try {
			Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			if (mExtras.containsKey(ResourceIdManager.SOUND_KEY)) {
				Object obj = mExtras.get(ResourceIdManager.SOUND_KEY);
				if (obj instanceof String) {
					String soundName = (String) obj;
					int soundId = mContext.getResources().getIdentifier(soundName, "raw", mContext.getPackageName());
					if (soundId != 0) {
						soundUri = Uri.parse("android.resource://" + mContext.getPackageName() + "/" + soundId);
					}
				}
			}
			mBuilder.setSound(soundUri);
		} catch (Exception e) {
			Log.d(mContext.getPackageName(), "set sound", e);
		}
	}

	private void setLargeIcon() {
		try {
			String resourceName = getStringFromBundle(ResourceIdManager.LARGE_ICON_KEY, null);
			if(resourceName != null) {
				Log.d(mContext.getPackageName(), resourceName);
				int id = mContext.getResources().getIdentifier(resourceName, ResourceIdManager.DRAWABLE, mContext.getPackageName());
				if (id != 0) {
					Drawable drawable = GetDrawable(id);
					mBuilder.setLargeIcon(((BitmapDrawable) drawable).getBitmap());
				} else {
					Log.e(mContext.getPackageName(), "resource not exitst: " + resourceName);
				}
			}
		} catch (Exception e) {
			Log.e(mContext.getPackageName(), "set large icon", e);
		}
	}
	
	@SuppressLint("NewApi")
	private Drawable GetDrawable(int id){
		if (Build.VERSION.SDK_INT >= 21) {
			return mContext.getResources().getDrawable(id, mContext.getTheme());
		} else {
			return mContext.getResources().getDrawable(id);
		}
	}
	
	private String getStringFromBundle(String key, String defaultValue) {
		if (mExtras.containsKey(key)) {
			Object obj = mExtras.get(key);
			if (obj instanceof String) {
				return (String) obj;
			}
		}
		return defaultValue;
	}

}
