package com.eva.notificationmanager;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.unity3d.player.UnityPlayer;

public class GCM {

    private static boolean isReceiverRegistered;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    public void startGCMService(final String senderId, final String authKey, final String langISO,
                                final String userData, final Boolean needCheckIsFirstTime) {
        UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(UnityPlayer.currentActivity);
                if(!needCheckIsFirstTime ||
                        !sharedPreferences.getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER,
                                false))
                    startGCMServiceInner(UnityPlayer.currentActivity, authKey, senderId,
                        langISO, userData);
            }
        });
    }

    private void startGCMServiceInner(Context context, final String authKey,
                                      final String senderId, final String langISO,
                                      final String userData) {
        System.out.println("startGCMService lang: " + langISO + " USER_DATA : " + userData);

        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(QuickstartPreferences.AUTH_KEY, authKey).apply();
        sharedPreferences.edit().putString(QuickstartPreferences.PROJECT_ID, senderId).apply();
        sharedPreferences.edit().putString(QuickstartPreferences.LANGUAGE, langISO).apply();
        sharedPreferences.edit().putString(QuickstartPreferences.USER_DATA, userData).apply();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                System.out.println("PUSH_AUTH_PARAMS : " + authKey + "    " + senderId);
            }
        };

        // Registering BroadcastReceiver
        registerReceiver(context);
        if (checkPlayServices(context)) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(context, RegistrationIntentService.class);
            context.startService(intent);
        }
    }

    private void registerReceiver(Context context){
        if(!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(context).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }

    private boolean checkPlayServices(Context context) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(context);
        System.out.println("RESULT_GOOGLE_SERVICES_CHECK  = " + resultCode);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog((Activity) context, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
            }
            return false;
        }
        return true;
    }
}
