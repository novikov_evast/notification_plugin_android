/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eva.notificationmanager;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "com.eva.notificationmanager.MyGcmListenerService";

    public static final String SMALL_ICON = "small_icon";
    public static final String LARGE_ICON = "large_icon";
    public static final String TITLE = "title";
    public static final String TEXT = "text";
    public static final String CONTENT_INFO = "content_info";
    public static final String AUTO_CANCEL = "auto_cancel";
    public static final String SOUND = "sound";
    public static final String LIGHT = "light";
    public static final String GROUP_KEY = "group_key";
    public static final String PUBLIC = "public";
    public static final String GROUP_SUMMARY =  "is_group_summary";
    public static final String ID = "id";
    public static final String LARGE_ICON_LOCAL = "large_icon_local";
    public static final String SUBTEXT = "subtext";
    private static final String ACCENT_COLOR = "accent_color";
    public static final String PRIORITY = "priority";


    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);
        Log.d(TAG, "Data : " + data.getCharSequence("data").toString());

        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }

        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */

        try {
            sendNotification(new JSONObject(data.getCharSequence("data").toString()));
        } catch (JSONException | NullPointerException e) {
            Log.d(TAG, "exception : " + e);
            e.printStackTrace();
        }
        // [END_EXCLUDE]
    }
    // [END receive_message]

    private void sendNotification(JSONObject data) throws JSONException {
        Log.d(TAG, "startSendingPush");
        Intent launchIntent = getPackageManager().getLaunchIntentForPackage(getPackageName());
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, launchIntent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);

        if(!data.isNull(SMALL_ICON) && !data.getString(SMALL_ICON).isEmpty()) {
            try {
                int identifier = getResources().getIdentifier(data.getString(SMALL_ICON), "drawable",
                        getPackageName());
                notificationBuilder.setSmallIcon(identifier);
            } catch (Exception e) {
                e.printStackTrace();
                int id = getResources().getIdentifier(ResourceIdManager.DEFAULT_ICON, ResourceIdManager.DRAWABLE, getPackageName());
                notificationBuilder.setSmallIcon(id);
            }
        } else {
        	int id = getResources().getIdentifier(ResourceIdManager.DEFAULT_ICON, ResourceIdManager.DRAWABLE, getPackageName());
        	notificationBuilder.setSmallIcon(id);
        }
        if(!data.isNull(SUBTEXT) && !data.getString(SUBTEXT).isEmpty()) {
            notificationBuilder.setSubText(data.getString(SUBTEXT));
        }
        if(!data.isNull(TITLE) && !data.getString(TITLE).isEmpty()) {
            notificationBuilder.setContentTitle(data.getString(TITLE));
        }
        if(!data.isNull(TEXT) && !data.getString(TEXT).isEmpty()) {
            notificationBuilder.setContentText(data.getString(TEXT));
        }
        if(!data.isNull(PRIORITY) && !data.getString(PRIORITY).isEmpty()) {
            notificationBuilder.setPriority(data.getInt(PRIORITY));
        }
        if(!data.isNull(CONTENT_INFO) && !data.getString(CONTENT_INFO).isEmpty()) {
            notificationBuilder.setContentInfo(data.getString(CONTENT_INFO));
        }
        if(!data.isNull(AUTO_CANCEL) && !data.getString(AUTO_CANCEL).isEmpty()) {
            notificationBuilder.setAutoCancel(data.getBoolean(AUTO_CANCEL));
        }

        if(!data.isNull(SOUND) && !data.getString(SOUND).isEmpty()) {
            try {
                int identifier = getResources().getIdentifier(data.getString(SOUND), "raw",
                        getPackageName());
                Uri customSound = Uri.parse("android.resource://" + getPackageName() + "/" + identifier);
                notificationBuilder.setSound(customSound);
            } catch (Exception e) {
                e.printStackTrace();
                notificationBuilder.setSound(defaultSoundUri);
            }
        } else {
            notificationBuilder.setSound(defaultSoundUri);
        }
        if(!data.isNull(LARGE_ICON_LOCAL) && !data.getString(LARGE_ICON_LOCAL).isEmpty()) {
            try {
                int identifier = getResources().getIdentifier(data.getString(LARGE_ICON_LOCAL), "drawable",
                        getPackageName());
                Drawable drawable;
                if(Build.VERSION.SDK_INT >= 21){
                    drawable = getResources().getDrawable(identifier, getTheme());
                } else {
                    drawable = getResources().getDrawable(identifier);
                }
                if (drawable != null) {
                    notificationBuilder.setLargeIcon(((BitmapDrawable)drawable).getBitmap());
                } else {
                    setDefaultLargeIcon(notificationBuilder);
                }
            } catch (Exception e) {
                e.printStackTrace();
                setDefaultLargeIcon(notificationBuilder);
            }
        }
        if (!data.isNull(ACCENT_COLOR) && !data.getString(ACCENT_COLOR).isEmpty()) {
            notificationBuilder.setColor(Color.parseColor("#" + data.getString(ACCENT_COLOR)));
        }
        if (!data.isNull(LIGHT) && !data.getString(LIGHT).isEmpty()) {
            notificationBuilder.setLights(Color.parseColor("#" + data.getString(LIGHT)), 100, 200);
        }
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if(!data.isNull(GROUP_KEY) && !data.getString(GROUP_KEY).isEmpty()) {
                notificationBuilder.setGroup(data.getString(GROUP_KEY));
            }
            if(!data.isNull(GROUP_SUMMARY) && !data.getString(GROUP_SUMMARY).isEmpty()) {
                notificationBuilder.setGroupSummary(data.optBoolean(GROUP_SUMMARY, false));
            }
        }
        if(!data.isNull(PUBLIC) && !data.getString(PUBLIC).isEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationBuilder.setVisibility(Notification.VISIBILITY_PUBLIC);
            }
        }
        final int notificationId;
        if(!data.isNull(ID) && !data.getString(ID).isEmpty()) {
            notificationId = data.getInt(ID);
        } else {
            notificationId = randInt(51, Integer.MAX_VALUE);
        }
        notificationBuilder.setContentIntent(pendingIntent);

        final NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        Log.d(TAG, "just before notification");
    }

    private void setDefaultLargeIcon(NotificationCompat.Builder notificationBuilder) {
        Drawable drawable = null;
        try {
        	int id = getResources().getIdentifier(ResourceIdManager.DEFAULT_ICON, ResourceIdManager.DRAWABLE, getPackageName());
        	if(id != 0){
	            if(Build.VERSION.SDK_INT >= 21){
	                drawable = getResources().getDrawable(id, getTheme());
	            } else {
	                drawable = getResources().getDrawable(id);
	            }
        	}else{
        		Log.e(getPackageName(), "cannot find resource: " + ResourceIdManager.DEFAULT_ICON);
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(drawable != null) {
            notificationBuilder.setLargeIcon(((BitmapDrawable)drawable).getBitmap());
        } else {
            Log.d(TAG, "large icon drawable is null!");
        }
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }
}
