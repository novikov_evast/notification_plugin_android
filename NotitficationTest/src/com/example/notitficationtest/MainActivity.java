package com.example.notitficationtest;

import org.json.JSONObject;

import android.support.v7.app.ActionBarActivity;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import com.eva.notificationmanager.local.*;;


@SuppressLint("NewApi")
public class MainActivity extends ActionBarActivity {

    private NotificationScheduler mPlugin;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
		mPlugin = new NotificationScheduler();
		
        Button initButton = (Button) findViewById(R.id.button1);
        initButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View sender) {
			}
		});
        
        Button scheduleButton = (Button) findViewById(R.id.button2);
        scheduleButton.setOnClickListener(new ScheduleButtonListener(this, mPlugin));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
