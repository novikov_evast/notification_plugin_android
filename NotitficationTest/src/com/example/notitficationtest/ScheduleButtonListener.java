package com.example.notitficationtest;

import org.json.JSONObject;

import com.eva.notificationmanager.local.NotificationScheduler;
import com.eva.notificationmanager.ResourceIdManager;

import android.app.Activity;
import android.util.Log;
import android.view.View;

public class ScheduleButtonListener implements View.OnClickListener {

	private final Activity mActivity;
	private final NotificationScheduler mPlugin;
	
	public ScheduleButtonListener(Activity activity, NotificationScheduler plugin){
		mActivity = activity;
		mPlugin = plugin;
	}

	@Override
	public void onClick(View arg0) {
		try{
			JSONObject json = new JSONObject();
			json.put(ResourceIdManager.AUTO_CANCEL_KEY, true);
			json.put(ResourceIdManager.SMALL_ICON_KEY, "icon_small");
			//json.put(ResourceIdManager.LARGE_ICON_KEY, "icon_big");
			json.put(ResourceIdManager.SOUND_KEY, "bug");
			Log.d(mActivity.getPackageName(), json.toString());
			mPlugin.scheduleNotification(mActivity, 5, json.toString());
		}
		catch(Exception e){
			Log.e("ScheduleButtonListener", "onClick", e);
		}
	}
	
	
}
